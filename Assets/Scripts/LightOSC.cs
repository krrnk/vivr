﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOSC : MonoBehaviour
{
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    float lightsOn;

    public Light light;

    public Color color1;
    public Color color2;

    //---------------- Functions ----------------
    // Use this for initialization
    void Start()
    {
        light = GetComponent<Light>();
        osc.SetAddressHandler("/lights", getLightsState);
    }

    // OSC

    void getLightsState(OscMessage message)
    {
        lightsOn = message.GetFloat(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (lightsOn != 0)
        {
            light.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

        }

    }
}
