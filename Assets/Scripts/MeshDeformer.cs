﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshDeformer : MonoBehaviour {
	//---------------- Variables ----------------
	Mesh deformingMesh;

	Vector3[] originalVertices, displacedVertices;

	Vector3[] vertexVelocities;

	public float springForce = 20f;

	public float damping = 5f;

	float uniformScale = 1f;

    OSC osc;

	//---------------- Functions ----------------
	// Use this for initialization
	void Start () {
		// To perform any deformation we need to access the mesh
		// once we have it, we can extract the original vertex positions
		deformingMesh = GetComponent<MeshFilter>().mesh;

		// Keep track of the displaced vertices during deformation
		originalVertices = deformingMesh.vertices;
		// Copy original vertices to displayed vertices
		displacedVertices = new Vector3[originalVertices.Length];
		for (int i = 0; i < originalVertices.Length; i++)
		{
			displacedVertices[i] = originalVertices[i];
		}

		// store velocity of each vertex
		vertexVelocities = new Vector3[originalVertices.Length];

	}
	
	// Update is called once per frame
	void Update () {
		// Just checking x because we are working with a sphere
		// in the other case we should use Vector3 and adjust dimensions separately
		uniformScale = transform.localScale.x;

		for (int i = 0; i < displacedVertices.Length; i++)
		{
			UpdateVertex(i);
		}

		deformingMesh.vertices = displacedVertices;
		deformingMesh.RecalculateNormals();
	}

	// Apply deforming force where the player is looking at
	public void AddDeformingForce(Vector3 point, float force)
	{
        point = transform.InverseTransformPoint(point);

        // Loop through all displaced vertices and appku
        // deforming force to each one independently
        for (int i = 0; i < displacedVertices.Length; i++)
		{
			AddForceToVertex(i, point, force);
		}

        Debug.DrawLine(Camera.main.transform.position, point);
	}
	
	// Know where the force is coming from
	void AddForceToVertex(int i, Vector3 point, float force)
	{
		Vector3 pointToVertex = displacedVertices[i] - point;

		// Calculate physics
		float attenuatedForce = force / (1f + pointToVertex.sqrMagnitude);
		float velocity = attenuatedForce * Time.deltaTime;

		// Get directions
		vertexVelocities[i] += pointToVertex.normalized * velocity;
	}

	// Update Vetices by adjusting their position
	void UpdateVertex(int i)
	{
		Vector3 velocity = vertexVelocities[i];

		// Pull back the deformation with spring physics
		// so it does not go to infinity
		Vector3 displacement = displacedVertices[i] - originalVertices[i];
		displacement *= uniformScale;
		velocity -= displacement * springForce * Time.deltaTime;
		velocity *= 1f - damping * Time.deltaTime; // Apply damping so it does not jump forth and back endlessly
		vertexVelocities[i] = velocity;


        displacedVertices[i] += velocity * (Time.deltaTime / uniformScale);
	}
}
