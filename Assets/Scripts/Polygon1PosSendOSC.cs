﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polygon1PosSendOSC : MonoBehaviour
{
    public OSC osc;

    float axisX;
    float axisY;
    float axisZ;

    // Use this for initialization
    void Start()
    {

    }

    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
        //  Get coordinates
        axisX = transform.position.x;
        axisY = transform.position.y;
        axisZ = transform.position.z;

        // OSC
        OscMessage oscMsg = new OscMessage();

        // set osc address
        oscMsg.address = "/polygon1Location";

        // Add values to the OSC array
        oscMsg.values.Add(axisX);
        oscMsg.values.Add(axisY);
        oscMsg.values.Add(axisZ);

        // Send the messages
        osc.Send(oscMsg);
    }
}
