﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentCamMovement : MonoBehaviour
{
    //---------- Variables ---------- 
    public OSC osc;
    
    float posX;
    float posY;
    float posZ;
    
    float rotX;
    float rotY;
    float rotZ;


    //---------- Functions ---------- 
    // Use this for initialization
    void Start()
    {
        // Set OSC addresses
        osc.SetAddressHandler("/environmentPosRot", OnReceiveRotPos);
    }

    // OSC Function
    void OnReceiveRotPos(OscMessage message){
        posX = message.GetFloat(0);
        posY = message.GetFloat(1);
        posZ = message.GetFloat(2);
        
        rotX = message.GetFloat(3);
        rotY = message.GetFloat(4);
        rotZ = message.GetFloat(5);
    }

    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
        transform.position = new Vector3(posX, posY, posZ); 
        transform.localEulerAngles = new Vector3(rotX, rotY, rotZ);       
    }

    // Update is called once per frame
    void Update()
    {

    }
}
