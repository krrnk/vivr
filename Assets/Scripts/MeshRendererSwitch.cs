﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshRendererSwitch : MonoBehaviour {
	//---------------- Variables ----------------
	public OSC osc;

	Renderer renderer;

	float rendererSwitch;

	//---------------- Functions ----------------
	// Use this for initialization
	void Start () {
		renderer = gameObject.GetComponent<Renderer>();
		renderer.enabled = false;

		// Set OSC addresses
        osc.SetAddressHandler("/polygonsSwitch", OnReceiveStringsSwitch);
	}

	void OnReceiveStringsSwitch(OscMessage message)
	{
		rendererSwitch = message.GetFloat(0);

		if(rendererSwitch == 1)
		{
			renderer.enabled = true;
		}
		else
		{
			renderer.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
