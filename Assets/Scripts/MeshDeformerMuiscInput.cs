﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerMusicInput : MonoBehaviour {
	//---------------- Variables ----------------
	public float force = 10f;

	public float forceOffset = 0.1f;

	//---------------- Functions ----------------
	// Use this for initialization
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {

		force = 95000 * Mathf.Cos(force);

		force += force;

		if (Input.GetMouseButton(0))
		{
			HandleInput();
		}
	}

	void HandleInput()
	{
		// Check where the player is pointing at
		Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);

		// Use physics engine to cast the ray and check if it has hit something
		RaycastHit hit;
		if (Physics.Raycast(inputRay, out hit))
		{
			MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
			if (deformer)
			{
				Vector3 point = hit.point;
				// Add offsett so the vertices are always pushed into the surface
				point += hit.normal * forceOffset;
				deformer.AddDeformingForce(point, force);
			}
		}
	}
}
