﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerItself : MonoBehaviour
{
    //---------------- Variables ----------------
    public float force;

    public float forceIndex;

    public float forceOffset = 0.1f;

    public OSC osc;

    float fft;
    float amp;

    // public GameObject cube;

    //---------------- Functions ----------------
    // Use this for initialization
    void Start()
    {
        // Set OSC addresses
        osc.SetAddressHandler("/fft3", OnReceiveFFT);
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
    }

    void OnReceiveFFT(OscMessage message)
    {
        fft = message.GetFloat(0);

        force = fft * forceIndex;
        // Debug.Log(fft);

    }

    void HandleInput()
    {
        // OSC
        OscMessage oscMsg = new OscMessage();

        // set osc address
        oscMsg.address = "/rayCastLocationItself";

        // Check where the player is pointing at
        // Ray inputRay = thisCamera.ScreenPointToRay(Input.mousePosition);

        // Use physics engine to cast the ray and check if it has hit something
        RaycastHit hit;
        Vector3 point;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
            if (deformer)
            {
             
                point = hit.point;
                // Debug.Log(point);
                //Debug.DrawLine(transform.position, point);
                
                // Vector3 point = hit.point;	
                // Add offsett so the vertices are always pushed into the surface
                point += hit.normal * forceOffset;
                // point += hit.normal * amp;
                deformer.AddDeformingForce(point, force);

                // OSC
                oscMsg.values.Add(point.x);
                oscMsg.values.Add(point.y);
                oscMsg.values.Add(point.z);
                oscMsg.values.Add(hit.distance);

                osc.Send(oscMsg);

            }
        }
    }
}
