﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerRightSendOSC : MonoBehaviour
{
    //---------- Variables ---------- 
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    //---------- Functions ---------- 
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

        // Instantiate OSC message on very framerate 
        // so the array keeps a constant size
        OscMessage oscMsgPos = new OscMessage();
        OscMessage oscMsgRot = new OscMessage();
        OscMessage oscMsgAxis = new OscMessage();
        OscMessage oscMsgTrig = new OscMessage();
        OscMessage oscMsgGrip = new OscMessage();

        // Set OSC address
        oscMsgPos.address = "/ControllerRPosition";
        oscMsgRot.address = "/ControllerRRotation";
        oscMsgAxis.address = "/ControllerRTrackpadAxis";
        oscMsgTrig.address = "/ControllerRTrigger";
        oscMsgGrip.address = "/ControllerRGrip";

        // Get position and Rotation
        // add the elements to the OSC array
        oscMsgPos.values.Add(this.transform.position.x); // add X values to the array 
        oscMsgPos.values.Add(this.transform.position.y); // add Y values to the array
        oscMsgPos.values.Add(this.transform.position.z); // add Z values to the array 

        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.x); // add X values to the array 
        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.y); // add Y values to the array
        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.z); // add Z values to the array 
        //oscMsgAxis.values.Add(0); // add X values to the array 

        // Get trackpad 
        if (Controller.GetAxis() != Vector2.zero)
        {
            // Print to console
            //Debug.Log(gameObject.name + Controller.GetAxis().x);

            // add the elements to the OSC array
            oscMsgAxis.values.Add(Controller.GetAxis().x); // add X values to the array 
            oscMsgAxis.values.Add(Controller.GetAxis().y); // add X values to the array 
            osc.Send(oscMsgAxis);
        }

        // Get trigger
        if (Controller.GetHairTrigger())
        {
            // Print to console
            //Debug.Log(gameObject.name + "trigger press");

            // Add the elements to the OSC array
            oscMsgTrig.values.Add(1); // add bool 
        }
        else
        {
            // Add the elements to the OSC  array
            oscMsgTrig.values.Add(0); // add bool 
        }

        // Get grip
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            // Print to console
            //Debug.Log(gameObject.name + "Grip press");

            // Add the elements to the OSC array
            oscMsgGrip.values.Add(1); // add bool 

        }
        else
        {
            // Add the elements to the OSC array
            oscMsgGrip.values.Add(0); // add bool 
        }

        // Send the message to the client
        osc.Send(oscMsgPos);
        osc.Send(oscMsgRot);
   
        osc.Send(oscMsgTrig);
        osc.Send(oscMsgGrip);
    }

}
