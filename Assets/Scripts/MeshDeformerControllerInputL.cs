﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    // Physics
    Vector3 controllerLastPostion;
    Vector3 velocity;
    Vector3 acceleration;

    public float force;

    float forcePolygon1;
    float forcePolygon2;
    float forcePolygon3;
    float forcePolygon4;

    public float forceIndex;

    public float forceOffset;

    // FFT
    float fft;
    float amp;

    // Access other ganme objects
    public GameObject floor;
    public GameObject ceiling;
    public GameObject wall1;
    public GameObject wall2;
    public GameObject wall3;
    public GameObject wall4;
    public GameObject polygon1;
    public GameObject polygon2;
    public GameObject polygon3;
    public GameObject polygon4;

    public GameObject theOtherController;

    public bool pressingGrip = false;
    public bool freezingFloor = false;
    public bool freezingCeiling = false;
    public bool freezingWall1 = false;
    public bool freezingWall2 = false;
    public bool freezingWall3 = false;
    public bool freezingWall4 = false;
    public bool freezingPolygon1 = false;
    public bool freezingPolygon2 = false;
    public bool freezingPolygon3 = false;
    public bool freezingPolygon4 = false;



    // Buffer Num OSC
    float bufnum = 0;

    // Freeze OSC var
    float freeze = 0;

    //---------------- Functions ----------------
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();

        
    }

    // Use this for initialization
    void Start () {
        // Declare OSC Address
        osc.SetAddressHandler("/fft1", OnReceiveFFT);
    }


   void OnReceiveFFT(OscMessage message)
    {
        fft = message.GetFloat(0);
   
        force = fft;
        forcePolygon1 = message.GetFloat(1);
        forcePolygon2 = message.GetFloat(2);
        forcePolygon3 = message.GetFloat(3);
        forcePolygon4 = message.GetFloat(4);

    }


    // Update is called once per frame
    void Update () {

        HandleInput();
        VelocityAccelaration();


    }

    void VelocityAccelaration ()
    {
        velocity = (transform.position - controllerLastPostion) / Time.deltaTime;
        controllerLastPostion = transform.position;
        acceleration = velocity / (Mathf.Pow(Time.deltaTime, 2));
        Debug.Log(acceleration);

        // OSC
        OscMessage oscMsg = new OscMessage();

        // set osc address
        oscMsg.address = "/ControllerLVelAcc";

        // add values
        oscMsg.values.Add(velocity.x);
        oscMsg.values.Add(velocity.y);
        oscMsg.values.Add(velocity.z);
        oscMsg.values.Add(acceleration.x);
        oscMsg.values.Add(acceleration.y);
        oscMsg.values.Add(acceleration.z);

        // Send the message to the client
        osc.Send(oscMsg);

    }


    void HandleInput()
    {
        // OSC
        OscMessage oscMsg = new OscMessage();
        OscMessage oscMsgCollider = new OscMessage();
        OscMessage oscMsgFreezer = new OscMessage();
        OscMessage oscMsgTrackpad = new OscMessage();
        OscMessage oscMsgTrigger = new OscMessage();

        // set osc address
        oscMsg.address = "/rayCastLocationL";
        oscMsgCollider.address = "/leftCollider";
        oscMsgFreezer.address = "/leftFreezer";
        oscMsgTrackpad.address = "/ControllerLTrackpad";
        oscMsgTrigger.address = "/ControllerLTrigger";
  
        // Get trigger
        if (Controller.GetHairTrigger())
        {
            // Add the elements to the OSC array
            oscMsgTrigger.values.Add(1);
        }
        else
        {
            // Add the elements to the OSC  array
            oscMsgTrigger.values.Add(0);
        }

        // use physics engine to cast the ray and check if it has hit something
        RaycastHit hit;

        Vector3 point;
        if (Physics.Raycast(trackedObj.transform.position, trackedObj.transform.forward, out hit))
        {
            MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
            Renderer renderer = hit.collider.GetComponent<Renderer>();

            point = hit.point;

            if (deformer)
            {
                //Vector3 point = hit.point;
                // add offsett so the vertices are always pushed into the surface
                point += hit.normal * forceOffset;

                // Deform the mesh
                if (hit.collider.gameObject == polygon1)
                {
                    deformer.AddDeformingForce(point, forcePolygon1);
                }

                if (hit.collider.gameObject == polygon2)
                {
                    deformer.AddDeformingForce(point, forcePolygon2);
                }

                if (hit.collider.gameObject == polygon3)
                {
                    deformer.AddDeformingForce(point, forcePolygon3);
                }

                if (hit.collider.gameObject == polygon4)
                {
                    deformer.AddDeformingForce(point, forcePolygon4);
                }

                if (hit.collider.gameObject == floor || hit.collider.gameObject == ceiling || hit.collider.gameObject == wall1 || hit.collider.gameObject == wall2 || hit.collider.gameObject == wall3 || hit.collider.gameObject == wall4)
                {
                    deformer.AddDeformingForce(point, force);
                }

            }

            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
            {
                pressingGrip = true;
            }
            else
            {
                pressingGrip = false;
            }

            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
            {
                if (hit.collider.gameObject == floor)
                {
                    floor.GetComponent<MeshDeformer>().springForce = 0;

                    freezingFloor = true;
                }
                else
                {
                    freezingFloor = false;
                }

                if (hit.collider.gameObject == ceiling)
                {
                    ceiling.GetComponent<MeshDeformer>().springForce = 0;

                    freezingCeiling = true;
                }
                else
                {
                    freezingCeiling = false;
                }

                if (hit.collider.gameObject == wall1)
                {
                    wall1.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall1 = true;
                }
                else
                {
                    freezingWall1 = false;
                }

                if (hit.collider.gameObject == wall2)
                {
                    wall2.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall2 = true;
                }
                else
                {
                    freezingWall2 = false;
                }

                if (hit.collider.gameObject == wall3)
                {
                    wall3.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall3 = true;
                }
                else
                {
                    freezingWall3 = false;
                }

                if (hit.collider.gameObject == wall4)
                {
                    wall4.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall4 = true;
                }
                else
                {
                    freezingWall4 = false;
                }

                if (hit.collider.gameObject == polygon1)
                {
                    polygon1.GetComponent<MeshDeformer>().springForce = 0;

                    freezingPolygon1 = true;
                }
                else
                {
                    freezingPolygon1 = false;
                }

                if (hit.collider.gameObject == polygon2)
                {
                    polygon2.GetComponent<MeshDeformer>().springForce = 0;

                    freezingPolygon2 = true;
                }
                else
                {
                    freezingPolygon2 = false;
                }

                if (hit.collider.gameObject == polygon3)
                {
                    polygon3.GetComponent<MeshDeformer>().springForce = 0;

                    freezingPolygon3 = true;
                }
                else
                {
                    freezingPolygon3 = false;
                }

                if (hit.collider.gameObject == polygon4)
                {
                    polygon4.GetComponent<MeshDeformer>().springForce = 0;

                    freezingPolygon4 = true;
                }
                else
                {
                    freezingPolygon4 = false;
                }

                freeze = 1;
            }
            else
            {
                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingFloor == false)
                {
                 
                    floor.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingCeiling == false)
                {
                    ceiling.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall1 == false)
                {
                    wall1.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall2 == false)
                {
                    wall2.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall3 == false)
                {
                    wall3.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall4 == false)
                {
                    wall4.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingPolygon1 == false)
                {
                    polygon1.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingPolygon2 == false)
                {
                    polygon2.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingPolygon3 == false)
                {
                    polygon3.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingPolygon4 == false)
                {
                   polygon4.GetComponent<MeshDeformer>().springForce = 20;
                }

                freeze = 0;
            }

            if (hit.collider.tag == "wall")
            {
                oscMsgCollider.values.Add(0);
                // Debug.Log(hit.collider.tag);
            }

            if (hit.collider.tag == "polygon" && renderer.enabled)
            {
                if (hit.collider.gameObject == polygon1)
                {
                    oscMsgCollider.values.Add(1);
                }

                if (hit.collider.gameObject == polygon2)
                {
                    oscMsgCollider.values.Add(2);
                }

                if (hit.collider.gameObject == polygon3)
                {
                    oscMsgCollider.values.Add(3);
                }

                if (hit.collider.gameObject == polygon4)
                {
                    oscMsgCollider.values.Add(4);
                }

            }

            // add the elements to the OSC array
            oscMsg.values.Add(point.x);
            oscMsg.values.Add(point.y);
            oscMsg.values.Add(point.z);
            oscMsg.values.Add(hit.distance);
            oscMsgFreezer.values.Add(freeze);
           

        }

        // add 0 to the OSC array
        // so supercollider doesn't get confused
        oscMsg.values.Add(0);
        oscMsg.values.Add(0);
        oscMsg.values.Add(0);
        oscMsg.values.Add(0);

        // This one already sends 0 if not touching
        oscMsgTrackpad.values.Add(Controller.GetAxis().y);

        // Send the message to the client
        osc.Send(oscMsg);
        osc.Send(oscMsgCollider);
        osc.Send(oscMsgFreezer);
        osc.Send(oscMsgTrackpad);
        osc.Send(oscMsgTrigger);

    }


}
