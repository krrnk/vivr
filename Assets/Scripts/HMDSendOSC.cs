﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HMDSendOSC : MonoBehaviour {
	 //---------- Variables ---------- 
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device HMD
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    //---------- Functions ---------- 
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
        // Instantiate OSC message on very framerate 
        // so the array keeps a constant size
        OscMessage oscMsgPos = new OscMessage();
        OscMessage oscMsgRot = new OscMessage();

        // Set OSC address
        oscMsgPos.address = "/HMDPosition";
        oscMsgRot.address = "/HMDRotation";

        // Add the elements to the array
        oscMsgPos.values.Add(this.transform.position.x); // add X values to the array 
        oscMsgPos.values.Add(this.transform.position.y); // add Y values to the array
        oscMsgPos.values.Add(this.transform.position.z); // add Z values to the array 

        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.x); // add X values to the array 
        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.y); // add Y values to the array
        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.z); // add Z values to the array 



        // Send the message to the client
        osc.Send(oscMsgPos);
        osc.Send(oscMsgRot);

    }
	
}
