﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonRotate : MonoBehaviour
{
    //---------------- Variables ----------------
    public OSC osc;

    float rotX;
    float rotY;
    float rotZ;
    //---------------- Functions ----------------
    // Use this for initialization
    void Start()
    {
        // Set OSC addresses
        osc.SetAddressHandler("/polygonsRotation", OnReceiveRotY);
    }

    void OnReceiveRotY(OscMessage message)
    {
        rotX = message.GetFloat(0);
        rotY = message.GetFloat(1);
        rotZ = message.GetFloat(2);
    }
    

    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
        transform.eulerAngles = new Vector3(rotX, rotY, rotZ);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
