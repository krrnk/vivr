﻿using UnityEngine;
using System.Collections;

public class Fractal : MonoBehaviour {

	private static Vector3[] childDirections = {
		Vector3.up,
		Vector3.right,
		Vector3.left,
		Vector3.forward,
		Vector3.back
	};

	private static Quaternion[] childOrientations = {
		Quaternion.identity,
		Quaternion.Euler(0f, 0f, -90f),
		Quaternion.Euler(0f, 0f, 90f),
		Quaternion.Euler(90f, 0f, 0f),
		Quaternion.Euler(-90f, 0f, 0f)
	};

	public Mesh[] meshes;
	public Material material;
	public int maxDepth;
	public float childScale;
	public float spawnProbability;
	public float maxRotationSpeed;
	public float maxTwist;
	
	private float rotationSpeed;
	private int depth;
	private Material[,] materials;

    // Rotation angles
    float alpha = 0f;
    float beta = 0f;
    float gamma = 0f;
    public float alphaFreq;
    public float betaFreq;
    public float alphaIndex;
    public float betaIndex;
    public float gammaIndex;

    // Random movement
    public bool waiting = false;
    Vector3 randomPosition;
    float moveDelay = 1; // smoothness


    private void InitializeMaterials () {
		materials = new Material[maxDepth + 1, 2];
		for (int i = 0; i <= maxDepth; i++) {
			float t = i / (maxDepth - 1f);
			t *= t;
			materials[i, 0] = new Material(material);
            materials[i, 0].color = Color.Lerp(Color.black, Color.white, t);
            materials[i, 1] = new Material(material);
            materials[i, 1].color = Color.Lerp(Color.grey, Color.cyan, t);
        }
		materials[maxDepth, 0].color = Color.white;
		materials[maxDepth, 1].color = Color.red;
	}
	
	private void Start () {
		rotationSpeed = Random.Range(-maxRotationSpeed, maxRotationSpeed);
		transform.Rotate(Random.Range(-maxTwist, maxTwist), 0f, 0f);
		if (materials == null) {
			InitializeMaterials();
		}
		gameObject.AddComponent<MeshFilter>().mesh =
			meshes[Random.Range(0, meshes.Length)];
		gameObject.AddComponent<MeshRenderer>().material =
			materials[depth, Random.Range(0, 2)];
		if (depth < maxDepth) {
			StartCoroutine(CreateChildren());
		}
	}

	private IEnumerator CreateChildren () {
		for (int i = 0; i < childDirections.Length; i++) {
			if (Random.value < spawnProbability) {
				yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
				new GameObject("Fractal Child").AddComponent<Fractal>().
					Initialize(this, i);
			}
		}
	}

	private void Initialize (Fractal parent, int childIndex) {
		meshes = parent.meshes;
		materials = parent.materials;
		maxDepth = parent.maxDepth;
		depth = parent.depth + 1;
		childScale = parent.childScale;
		spawnProbability = parent.spawnProbability;
		maxRotationSpeed = parent.maxRotationSpeed;
		maxTwist = parent.maxTwist;
		transform.parent = parent.transform;
		transform.localScale = Vector3.one * childScale;
		transform.localPosition =
			childDirections[childIndex] * (0.5f + 0.5f * childScale);
		transform.localRotation = childOrientations[childIndex];
	}

    // Generate a random vector for movement
    IEnumerator LerpCube()
    {
        randomPosition = new Vector3(Random.Range(1, -1), Random.Range(1, -1), Random.Range(1, -1));
        waiting = true;
        yield return new WaitForSeconds(moveDelay);
        waiting = false;
        //Debug.Log ("waited");
    }

    void FixedUpdate()
    {

        // Wait for X time for the cube to move
        if (waiting == false)
        {
            SendMessage("LerpCube");
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, randomPosition, Time.deltaTime * 1);
            // transform.position = new Vector3(-0.96f, 0.619f, Time.deltaTime * 1f);
        }
    }

    private void Update () {
	    // Update angles
        alpha += alphaFreq;
        beta += betaFreq;

        float x = alphaIndex * Mathf.Cos(alpha);
        float y = betaIndex * Mathf.Sin(beta);
        float z = gammaIndex * (alpha + Mathf.Atan2(y, x));

        // Rotate
        transform.Rotate(x * Time.deltaTime, y * Time.deltaTime, z * Time.deltaTime);
    }
}