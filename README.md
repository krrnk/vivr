# VIVR

This repository supports the Master Thesis "Vibrating Instruments in Virtual Reality: A cohesive approach to the design of Virtual Reality Musical Instruments".

## Installation
### Unity 2018.2 up
The project can be run straight from the Unity editor.

### SuperCollider 3.9.3 up

#### ATK for SuperCollider
SuperCollider needs this dependecy.

Follow the installation instructions from [(http://www.ambisonictoolkit.net/download/supercollider/)]

#### Samples
Add 8 samples to the /SuperCollider folder.

Replace lines 32-39 of vivr.scd with the names of your own samples.

```
    ~path1 = PathName(thisProcess.nowExecutingPath).parentPath ++ "taapet.wav";
	~path2 = PathName(thisProcess.nowExecutingPath).parentPath ++ "carl.wav";
	~path3 = PathName(thisProcess.nowExecutingPath).parentPath ++ "wavxtsy.wav";
	~path4 = PathName(thisProcess.nowExecutingPath).parentPath ++ "bartok.wav";
	~path5 = PathName(thisProcess.nowExecutingPath).parentPath ++ "rap.wav";
	~path6 = PathName(thisProcess.nowExecutingPath).parentPath ++ "devil.wav";
	~path7 = PathName(thisProcess.nowExecutingPath).parentPath ++ "destructo.wav";
	~path8 = PathName(thisProcess.nowExecutingPath).parentPath ++ "yellow.wav";
```

## Running the project
First, run or build the project in Unity.

Second, select your HTC-Vive headset as the default audio system from Windows Sound Settings.

Last, evalaute the code of /SuperCollider/vivr.scd with ctrl+enter.